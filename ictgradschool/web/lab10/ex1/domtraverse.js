"use strict";

$("div:first").css("background-color","pink");

$("#footer").css("color","red");

$("div:eq(1)").css("visibility","hidden");
// $("div:eq(1)").hide();

$("small:first-child").prepend("Hello World");

$("#makeBold").click(function () {
    $("p").css("font-weight", "bold")
});
